package stack

import "fmt"

type Stack struct {
	head *element
	len  int
}

type element struct {
	next *element
	data interface{}
}

func Init(size int) *Stack {
	return &Stack{
		len: size,
	}
}

// Push add new element to stack
func (s *Stack) Push(data interface{}) {
	ele := &element{
		data: data,
	}

	if s.head == nil {
		s.head = ele
	} else {
		ele.next = s.head
		s.head = ele
	}
	s.len++
	return
}

// Pop fetch first element from stack
func (s *Stack) Pop() (interface{}, error) {
	if !s.IsStackEmpty() {
		last := s.head
		if last.next != nil {
			s.head = last.next
			s.len--
			return last.data, nil
		} else {
			s.head = nil
		}
		s.len--
		return last.data, nil
	}
	return nil, fmt.Errorf("stack is empty")
}

// Top get data element (not fetching from stack)
func (s *Stack) Top() (interface{}, error) {
	data, err := s.Pop()
	if err != nil {
		return nil, err
	}
	s.Push(data)
	return data, nil
}

func (s *Stack) IsStackEmpty() bool {
	if s.head == nil {
		return true
	}
	return false
}
