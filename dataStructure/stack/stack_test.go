package stack

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

const (
	stackSizeDefault = 10
)

func TestRepositorySuite(t *testing.T) {
	suite.Run(t, new(TestSuite))
}

type TestSuite struct {
	suite.Suite
}

func (s *TestSuite) TestStack() {
	s.Run("test stack: positive case", func() {
		stack := Init(stackSizeDefault)

		for i := 0; i < stackSizeDefault; i++ {
			stack.Push(i)
		}

		for i := 0; i < stackSizeDefault; i++ {
			data, err := stack.Pop()
			s.Equal(stackSizeDefault-1-i, data)
			s.NoError(err)
		}

		s.Equal(true, stack.IsStackEmpty())
	})
}
