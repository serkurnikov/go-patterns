package graph

import (
	"fmt"
	"github.com/stretchr/testify/suite"
	"testing"
)

const (
	vertexSizeDefault = 11
)

func TestRepositorySuite(t *testing.T) {
	suite.Run(t, new(TestSuite))
}

type TestSuite struct {
	suite.Suite
}

func (s *TestSuite) TestDFS() {
	s.Run("test stack: positive case", func() {
		g := NewDirectedGraph()

		for i := 1; i < vertexSizeDefault; i++ {
			g.AddVertex(i)
		}

		g.AddEdge(1, 9)
		g.AddEdge(1, 5)
		g.AddEdge(1, 2)
		g.AddEdge(2, 2)
		g.AddEdge(3, 4)
		g.AddEdge(5, 6)
		g.AddEdge(5, 8)
		g.AddEdge(6, 7)
		g.AddEdge(9, 10)

		visitedOrder := make([]int, 0)
		cb := func(i int) {
			visitedOrder = append(visitedOrder, i)
		}
		DFS(g, g.Vertices[1], cb)

		// add assertions here
		fmt.Println(visitedOrder)
	})
}
