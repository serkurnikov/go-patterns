# Graph 

## Depth-first search (DFS)

Depth-first search (DFS) is an algorithm for traversing or searching tree or graph data structures. 
The algorithm starts at the root node (selecting some arbitrary node as the root node in the case of a graph) 
and explores as far as possible along each branch before backtracking. Extra memory, usually a stack, is needed 
to keep track of the nodes discovered so far along a specified branch which helps in backtracking of the graph.

![graph_1.png](..%2F..%2Fassets%2Fgraph_1.png)

A version of depth-first search was investigated in the 19th century by French mathematician
Charles Pierre Trémaux as a strategy for solving mazes.

The DFS algorithm works as follows:

- Start by putting any one of the graph's vertices on top of a stack.
- Take the top item of the stack and add it to the visited list.
- Create a list of that vertex's adjacent nodes. Add the ones which aren't in the visited list to the top of the stack.
- Keep repeating steps 2 and 3 until the stack is empty.

![graph_2.png](..%2F..%2Fassets%2Fgraph_2.png)

# (BFS)