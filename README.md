
## Design patterns with Golang examples

This is a collection of 22 popular design patterns with Go code examples and a brief description of the pattern.

Brief descriptions will use classical terms such as Class, Object, Abstract Class. Applicable to the Go language, 
these are the Type, the Value of that type, and the Interface (where possible).

Knowing how to use design patterns correctly, in the right place at the right time, so to speak, will help save your nerves.

## Setup

You can download this repository and run tests

```bash
$ go get gitlab.com/serkurnikov/go-patterns
```

## Паттерны

### [Порождающие (Creational)](creational)

* [Абстрактная фабрика (Abstract Factory)](creational/abstractFactory)
* [Строитель (Builder)](creational/builder)
* [Фабричный метод (Factory Method)](creational/factoryMethod)
* [Прототип (Prototype)](creational/prototype)
* [Одиночка (Singleton)](creational/singleton)

### [Структурные (Structural)](structural)

* [Адаптер (Adapter)](structural/Adapter)
* [Мост (Bridge)](structural/Bridge)
* [Компоновщик (Composite)](structural/Composite)
* [Декоратор (Decorator)](structural/Decorator)
* [Фасад (Facade)](structural/Facade)
* [Приспособленец (Flyweight)](structural/Flyweight)
* [Заместитель (Proxy)](structural/Proxy)

### [Поведенческие (Behavioral)](behavioral)

* [Цепочка ответственности (Chain Of Responsibility)](behavioral/ChainOfResponsibility)
* [Команда (Command)](behavioral/Command)
* [Итератор (Iterator)](behavioral/Iterator)
* [Посредник (Mediator)](behavioral/Mediator)
* [Хранитель (Memento)](behavioral/Memento)
* [Наблюдатель (Observer)](behavioral/Observer)
* [Состояние (State)](behavioral/State)
* [Стратегия (Strategy)](behavioral/Strategy)
* [Шаблонный метод (Template Method)](behavioral/TemplateMethod)
* [Посетитель (Visitor)](behavioral/Visitor)
