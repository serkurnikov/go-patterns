PROJECT_NAME := "go-patterns"
PKG := $(PROJECT_NAME)
PKG_LIST := $(shell go list ./... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint: dep ## Lint testing
	@go fmt ./...
	@golangci-lint run -v

test: ## Run unittests
	@go test -short ${PKG_LIST}

prof: ##profiling application
	sh ./scripts/profile.sh;

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

cover: ## Generate global code coverage report
	@go test ./... -coverprofile=coverage.out -coverpkg=./...
	@go tool cover -html=coverage.out -o cover.html
	@rm -rf ./coverage.out

dep: ## Get the dependencies
	@go env -w GOPRIVATE=gitlab.com/serkurnikov
	@git config --global http.sslVerify false
	@go get -v -d ./...
	@go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.44.0

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
